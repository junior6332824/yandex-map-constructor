
<!--  ************************************        карта           ************************************     -->



<script src="https://api-maps.yandex.ru/2.1/?lang=ru-RU&amp;apikey=Ваш ключ" type="text/javascript"></script>
	
	
<div id="myMap" style="width: 700px; height: 700px;"></div>

<script>

ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map ('myMap', {
        center: [54.20, 37.64],
        zoom: 15
    }),
	
	
	
	myGeoObject = new ymaps.GeoObject({
	});
	myMap.geoObjects
	.add(myGeoObject)
	.add(new ymaps.Placemark([54.205452, 37.646974], {
		balloonContentHeader: "Точка №1",
		balloonContentBody: "Body",
		balloonContentFooter: "Footer",
		hintContent: "Точка №1" 
	},{
		present: "islands#dotIcon",
		iconColor: "#d50f0a",
	})),
		
	myGeoObject = new ymaps.GeoObject({
	});
	myMap.geoObjects
	.add(myGeoObject)
	.add(new ymaps.Placemark([54.198184, 37.639446], {
		balloonContentHeader: "Точка №2",
		balloonContentBody: "Body",
		balloonContentFooter: "Footer",
		hintContent: "Точка №1" 
	},{
		present: "islands#dotIcon",
		iconColor: "#0060b2",
	}));
		

}
</script>


<!--  ************************************        карта выделение облости           ************************************     -->


<script>

  var mordoviya = ymaps.geoQuery(ymaps.regions.load("RU", {
        lang: "ru"
    })).search('properties.hintContent = "Республика Алтай"').setOptions({
        fillOpacity: '0.4',
        fillColor: '#1ab7d8',
        strokeColor: '#28a745'
		
    });
	balloonContentHeader: "Калужская область , Юхновский район.",
    mordoviya.addToMap(myMap);


</script>






<!--  ************************************        Карта с иконкой и 2 точками со слайдером        ************************************     -->



<script src="https://api-maps.yandex.ru/2.1/?lang=ru-RU&amp;apikey=ключ" type="text/javascript"></script>
	
	
<div id="myMap" style="width: 100%; height: 700px;"></div>

<script>

ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map ('myMap', {
        center: [57.63, 56.38],
        zoom: 4
    }),
	

	myGeoObject = new ymaps.GeoObject({
	});
	myMap.geoObjects
	.add(myGeoObject)
	.add(new ymaps.Placemark([54.726030, 35.341176], {
		balloonContentHeader: "Калужская область , Юхновский район.",
		balloonContentBody: '<div  class="slider" ><div class="item"><img  src="/kontakty/pic/1.jpg"/></div> <div class="item"><img class="" src="/kontakty/pic/2.jpg"/></div> <div class="item"><img class="" src="/kontakty/pic/3.jpg"/></div> <div class="item"><img class="" src="/kontakty/pic/4.jpg"/></div>    <a class="prev" onclick="minusSlide()">&#10094;</a><a class="next" onclick="plusSlide()">&#10095;</a></div>',
		balloonContentFooter: "Текст."
	},{
            iconLayout: 'default#image',
            iconImageHref: '/kontakty/logo.png',
			hideIconOnBalloonOpen: false,
            iconImageSize: [180, 100]
        }
	)),
	

	myGeoObject = new ymaps.GeoObject({
	});
	myMap.geoObjects
	.add(myGeoObject)
	.add(new ymaps.Placemark([54.735147, 55.958727], {
		balloonContentHeader: "Башкортостан",
		balloonContentBody: '<div  class="slider" ><div class="item"><img  src="/kontakty/pic3/1.jpg"/></div> <div class="item"><img class="" src="/kontakty/pic3/2.jpg"/></div> <div class="item"><img class="" src="/kontakty/pic3/3.jpg"/></div> <div class="item"><img class="" src="/kontakty/pic3/4.jpg"/></div> <div class="item"><img class="" src="/kontakty/pic3/5.jpg"/></div> <div class="item"><img class="" src="/kontakty/pic3/6.jpg"/></div> <div class="item"><img class="" src="/kontakty/pic3/7.jpg"/></div> <div class="item"><img class="" src="/kontakty/pic3/8.jpg"/></div>    <a class="prev" onclick="minusSlide()">&#10094;</a><a class="next" onclick="plusSlide()">&#10095;</a></div>',
		balloonContentFooter: "Абзелиловский район, деревня Аслаево"
	},{
		present: "islands#dotIcon",
		iconColor: "#4a8716",
		}
	)),
	

	myGeoObject = new ymaps.GeoObject({
	});
	myMap.geoObjects
	.add(myGeoObject)
	.add(new ymaps.Placemark([51.958182, 85.960373], {
		balloonContentHeader: "Республика Алтай",
		balloonContentBody: '<div  class="slider"><div class="item"><img src="/kontakty/pic2/1.jpg"/></div><div class="item"><img src="/kontakty/pic2/2.jpg"/></div><div class="item"><img src="/kontakty/pic2/4.jpg"/></div><div class="item"><img src="/kontakty/pic2/5.jpg"/></div><div class="item"><img src="/kontakty/pic2/6.jpg"/></div><div class="item"><img src="/kontakty/pic2/7.jpg"/></div><div class="item"><img src="/kontakty/pic2/8.jpg"/></div><div class="item"><img src="/kontakty/pic2/9.jpg"/></div><div class="item"><img src="/kontakty/pic2/10.jpg"/></div><div class="item"><img src="/kontakty/pic2/11.jpg"/></div><div class="item"><img src="/kontakty/pic2/12.jpg"/></div><div class="item"><img src="/kontakty/pic2/13.jpg"/></div>     <a class="prev" onclick="minusSlide()">&#10094;</a><a class="next" onclick="plusSlide()">&#10095;</a></div>',
		balloonContentFooter: " Белокуриха летом +38,1 зимой −36,5 <br>Кош-агачский район  летом +14.7° зимой −25.2° <br> Нижнекаменка  +22,5°  зимой -12,4°",
		hintContent: "Точка №1" 
	},{
		present: "islands#dotIcon",
		iconColor: "#4a8716",
	}));
		

}
</script>
